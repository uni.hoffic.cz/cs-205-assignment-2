% Group 40
% 963810 - Petr Hoffmann
% 963560 - Krzysztof Bielikowicz
% 965677 - Mateusz Meller
% This is the group’s own work.





parent(queenmother,elisabeth). parent(elisabeth,charles).
parent(elisabeth,andrew). parent(elisabeth,anne).
parent(elisabeth,edward). parent(diana,william).
parent(diana,harry). parent(sarah,beatrice).
parent(anne,peter). parent(anne,zara).
parent(george,elisabeth). parent(philip,charles).
parent(philip,andrew). parent(philip,edward).
parent(charles,william). parent(charles,harry).
parent(andrew,beatrice). parent(andrew,eugene).
parent(mark,peter). parent(mark,zara).
parent(william,georgejun). parent(kate,georgejun).
parent(william,charlotte). parent(kate,charlotte).
parent(philip,anne). parent(william,louis).
parent(kate,louis).

female(queenmother).
female(elisabeth).
female(diana).
female(anne).
female(beatrice).
female(charlotte).
female(kate).
female(sarah).
female(zara).

male(charles).
male(andrew).
male(edward).
male(william).
male(harry).
male(peter).
male(george).
male(philip).
male(charles).
male(andrew).
male(eugene).
male(mark).
male(georgejun).
male(louis).

% Q1.1a
the_royal_females([]).
the_royal_females([H|T]) :- female(H), the_royal_females(T).

% Q1.1b
the_royal_males([]).
the_royal_males([H|T]) :- male(H), the_royal_males(T).

% Q1.2
the_royal_family([]).
the_royal_family([H|T]) :- (the_royal_males([H]) ; the_royal_females([H])), the_royal_family(T).

% Q1.3
mother(X, Y) :- female(X), parent(X,Y).

% Q1.4
ancestor(X, Y) :- parent(X,Y).
ancestor(X, Y) :- parent(X, Z), ancestor(Z, Y).

% Q1.5
sibling(X, Y) :- parent(Z, X), parent(Z, Y).

% Q1.6
brother(X, Y) :- sibling(X, Y), male(X).

% Q1.7
%?- ancestor(X, louis).
%X = william ;
%X = kate ;
%X = queenmother ;
%X = elisabeth ;
%X = diana ;
%X = george ;
%X = philip ;
%X = charles ;
%false.

% Q1.8
grandmother(X, Y) :- female(X), parent(X, Z), parent(Z, Y).

%?- grandmother(queenmother, Y).
%Y = charles ;
%Y = andrew ;
%Y = anne ;
%Y = edward.

% Q1.9
common_ancestors([], X).
common_ancestors([H|T], X) :- ancestor(H, X), common_ancestors(T, X).

%?- common_ancestors([anne,edward], X).
%false.

%?- common_ancestors([diana, charles],X).
%X = william ;
%X = harry ;
%X = georgejun ;
%X = charlotte ;
%X = louis ;
%false.

% Q1.10
has_brother_who_is_granddad(X) :- brother(X, Y), male(Y), parent(Y, A), parent(A, B).




% Q2a
nth_elt(0, [H|T], H, T) :- !.
nth_elt(N, [H|T], E, [H|T2]) :-
    NewN is N - 1,
    nth_elt(NewN, T, E, T2).

nth_elt_with_test(N, L, E, R) :-
    N > 0, length(L, Lgth),
    N =< Lgth,
    nth_elt(N, L, E, R).


% Question 2b doesn't restrict us from using libraries. In fact, the exact
% predicate we're supposed to define is defined in the official swipl
% documentation:
% http://www.swi-prolog.org/pldoc/man?predicate=sort/2
% It's also autoloaded, which means defining my own sort/2 procedure results
% in the following error:
% No permission to modify static procedure `sort/2'
% Here is another solution using insertion sort.
insertX(X, [], [X]) :- !.
insertX(X, [H|T], [X, H|T]) :- X =< H, !.
insertX(X, [H|T], [H|L]) :- insertX(X, T, L).

insertionSort([], []) :- !.
insertionSort([H|T], R) :- insertionSort(T, RR), insertX(H, RR, R).

% Q2c
medianHelp(Length, Sorted, X) :-
    % check if the length is even
    Center is Length div 2,
    Length_CHECK is (Center *2 + 1),
    Length =\= Length_CHECK,
    % calculate the average of two middle elements
    Lower_Center is Center - 1,
    Higher_Center is Center,
    nth_elt_with_test(Lower_Center, Sorted, Lower, L),
    nth_elt_with_test(Higher_Center, Sorted, Higher, L2),
    X is (Lower + Higher) / 2.
medianHelp(Length, Sorted, X) :-
    % check if the length is odd
    Center is Length div 2,
    Length_CHECK is (Center*2 + 1),
    Length =:= Length_CHECK,
    % take the middle element
    nth_elt_with_test(Center, Sorted, X, L).
median(L,X) :- insertionSort(L,Sorted), length(Sorted,Length), medianHelp(Length, Sorted, X).







% Q3a
euclidsqr([], [], 0).
euclidsqr([A|AL], [B|BL], NewED) :- euclidsqr(AL, BL, ED), NewED is ((A - B) * (A - B) + ED).

% Q3b
euclidsqr_acc([], [], ACC, ACC).
euclidsqr_acc([A|AL], [B|BL], ACC, ED) :- NEW_ACC is ((A - B) * (A - B) + ACC), euclidsqr_acc(AL, BL, NEW_ACC, ED).






% Q4a
member_rem_acc(E, [], Acc, Acc).
member_rem_acc(E, [E|T], Acc, R) :- member_rem_acc(E, T, Acc, R).
member_rem_acc(E, [H|T], Acc, R) :- E \= H, append(Acc, [H], List), member_rem_acc(E, T, List, R).
member_rem(E, L, R) :- member_rem_acc(E, L, [], R).

% Q4b
gen_list_acc(0, Acc, Acc, _) :- !.
gen_list_acc(N, L, Acc, [H|T]) :-
    \+ member(H,T),
    NewN is N - 1,
    append(Acc, [H], NewAcc),
    gen_list_acc(NewN, L, NewAcc, T).
gen_list_acc(N, L, Acc, [H|T]) :-
    member(H, T), NewN is N - 1,
    member_rem(H, T, NewTail),
    append(Acc, [H], NewAcc),
    gen_list_acc(NewN, L, NewAcc, NewTail).
gen_list(N, L, D) :- gen_list_acc(N, L, [], D).

% Q4x
solve_sujiko_1([X1,X2,X3,X4,X5,X6,X7]) :-
%check if all arguments are [1,3,4,5,6,8,9]
    gen_list(7, PossibleNumbers, [1,3,4,5,6,8,9]),
    member(X1, PossibleNumbers),
    member(X2, PossibleNumbers),
    member(X3, PossibleNumbers),
    member(X4, PossibleNumbers),
    member(X5, PossibleNumbers),
    member(X6, PossibleNumbers),
    member(X7, PossibleNumbers),
%check if all arguments are distinct
    gen_list(7, DistinctElements, [X1,X2,X3,X4,X5,X6,X7]),
    length(DistinctElements, Lgth),
    Lgth =:= 7,
    X1 + X2 + X4 + X5 =:= 23,
    X2 + X3 + X5 + X6 =:= 27,
    X4 + X5 + X7 =:= 10,
    X5 + X6 + X7 =:= 15.

%?- solve_sujiko_1([X1,X2,X3,X4,X5,X6,X7]).
%X1 = 5,
%X2 = 9,
%X3 = 4,
%X4 = 3,
%X5 = 6,
%X6 = 8,
%X7 = 1 ;
%false.

solve_sujiko_2([X1,X2,X3,X4,X5,X6,X7]) :-
%check if all arguments are [1,2,3,4,6,8,9]
    gen_list(7, PossibleNumbers, [1,2,3,4,6,8,9]),
    member(X1, PossibleNumbers),
    member(X2, PossibleNumbers),
    member(X3, PossibleNumbers),
    member(X4, PossibleNumbers),
    member(X5, PossibleNumbers),
    member(X6, PossibleNumbers),
    member(X7, PossibleNumbers),
%check if all arguments are distinct
    gen_list(7, DistinctElements, [X1,X2,X3,X4,X5,X6,X7]),
    length(DistinctElements, Lgth),
    Lgth =:= 7,
    X1 + X2 + X3 =:= 16,
    X2 + X3 + X4 =:= 15,
    X3 + X5 + X6 =:= 19,
    X3 + X4 + X6 + X7 =:= 14.

%    ?- solve_sujiko_2([X1,X2,X3,X4,X5,X6,X7]).
%    X1 = 2,
%    X2 = 8,
%    X3 = 6,
%    X4 = 1,
%    X5 = 9,
%    X6 = 4,
%    X7 = 3 ;
%    false.


% Q4d
solve_sub(X1, X2, X3, X4, Sum) :- X1 + X2 + X3 + X4 =:= Sum.

new_solve_sujiko2([X11,X12,X22,X23,X31,X32,X33]) :-
%check if all arguments are [1,2,3,4,6,8,9]
    gen_list(7, PossibleNumbers, [1,2,3,4,6,8,9]),
    member(X11, PossibleNumbers),
    member(X12, PossibleNumbers),
    member(X22, PossibleNumbers),
    member(X23, PossibleNumbers),
    member(X31, PossibleNumbers),
    member(X32, PossibleNumbers),
    member(X33, PossibleNumbers),
%check if all arguments are distinct
    gen_list(7, DistinctElements, [X11,X12,X22,X23,X31,X32,X33]),
    length(DistinctElements, Lgth),
    Lgth =:= 7,
%check solutions
    solve_sub(X11,X12,5,X22,21),
    solve_sub(X22,7,X12,X23,22),
    solve_sub(5,X22,X31,X32,24),
    solve_sub(X22,X23,X32,X33,14).







