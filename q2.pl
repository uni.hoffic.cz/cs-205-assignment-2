% Q2a
nth_elt(0, [H|T], H, T) :- !.
nth_elt(N, [H|T], E, [H|T2]) :-
    NewN is N - 1,
    nth_elt(NewN, T, E, T2).

nth_elt_with_test(N, L, E, R) :-
    N > 0, length(L, Lgth),
    N =< Lgth,
    nth_elt(N, L, E, R).


% Question 2b doesn't restrict us from using libraries. In fact, the exact
% predicate we're supposed to define is defined in the official swipl
% documentation:
% http://www.swi-prolog.org/pldoc/man?predicate=sort/2
% It's also autoloaded, which means defining my own sort/2 procedure results
% in the following error:
% No permission to modify static procedure `sort/2'
% Here is another solution using insertion sort.
insertX(X, [], [X]) :- !.
insertX(X, [H|T], [X, H|T]) :- X =< H, !.
insertX(X, [H|T], [H|L]) :- insertX(X, T, L).

insertionSort([], []) :- !.
insertionSort([H|T], R) :- insertionSort(T, RR), insertX(H, RR, R).

% Q2c
medianHelp(Length, Sorted, X) :-
    % check if the length is even
    Center is Length div 2,
    Length_CHECK is (Center *2 + 1),
    Length =\= Length_CHECK,
    % calculate the average of two middle elements
    Lower_Center is Center - 1,
    Higher_Center is Center,
    nth_elt_with_test(Lower_Center, Sorted, Lower, L),
    nth_elt_with_test(Higher_Center, Sorted, Higher, L2),
    X is (Lower + Higher) / 2.
medianHelp(Length, Sorted, X) :-
    % check if the length is odd
    Center is Length div 2,
    Length_CHECK is (Center*2 + 1),
    Length =:= Length_CHECK,
    % take the middle element
    nth_elt_with_test(Center, Sorted, X, L).
median(L,X) :- insertionSort(L,Sorted), length(Sorted,Length), medianHelp(Length, Sorted, X).
