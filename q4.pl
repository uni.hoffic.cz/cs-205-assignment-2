% Q4a
member_rem_acc(E, [], Acc, Acc).
member_rem_acc(E, [E|T], Acc, R) :- member_rem_acc(E, T, Acc, R).
member_rem_acc(E, [H|T], Acc, R) :- E \= H, append(Acc, [H], List), member_rem_acc(E, T, List, R).
member_rem(E, L, R) :- member_rem_acc(E, L, [], R).

% Q4b
gen_list_acc(0, Acc, Acc, _) :- !.
gen_list_acc(N, L, Acc, [H|T]) :-
    \+ member(H,T),
    NewN is N - 1,
    append(Acc, [H], NewAcc),
    gen_list_acc(NewN, L, NewAcc, T).
gen_list_acc(N, L, Acc, [H|T]) :-
    member(H, T), NewN is N - 1,
    member_rem(H, T, NewTail),
    append(Acc, [H], NewAcc),
    gen_list_acc(NewN, L, NewAcc, NewTail).
gen_list(N, L, D) :- gen_list_acc(N, L, [], D).

% Q4x
solve_sujiko_1([X1,X2,X3,X4,X5,X6,X7]) :-
%check if all arguments are [1,3,4,5,6,8,9]
    gen_list(7, PossibleNumbers, [1,3,4,5,6,8,9]),
    member(X1, PossibleNumbers),
    member(X2, PossibleNumbers),
    member(X3, PossibleNumbers),
    member(X4, PossibleNumbers),
    member(X5, PossibleNumbers),
    member(X6, PossibleNumbers),
    member(X7, PossibleNumbers),
%check if all arguments are distinct
    gen_list(7, DistinctElements, [X1,X2,X3,X4,X5,X6,X7]),
    length(DistinctElements, Lgth),
    Lgth =:= 7,
    X1 + X2 + X4 + X5 =:= 23,
    X2 + X3 + X5 + X6 =:= 27,
    X4 + X5 + X7 =:= 10,
    X5 + X6 + X7 =:= 15.

%?- solve_sujiko_1([X1,X2,X3,X4,X5,X6,X7]).
%X1 = 5,
%X2 = 9,
%X3 = 4,
%X4 = 3,
%X5 = 6,
%X6 = 8,
%X7 = 1 ;
%false.

solve_sujiko_2([X1,X2,X3,X4,X5,X6,X7]) :-
%check if all arguments are [1,2,3,4,6,8,9]
    gen_list(7, PossibleNumbers, [1,2,3,4,6,8,9]),
    member(X1, PossibleNumbers),
    member(X2, PossibleNumbers),
    member(X3, PossibleNumbers),
    member(X4, PossibleNumbers),
    member(X5, PossibleNumbers),
    member(X6, PossibleNumbers),
    member(X7, PossibleNumbers),
%check if all arguments are distinct
    gen_list(7, DistinctElements, [X1,X2,X3,X4,X5,X6,X7]),
    length(DistinctElements, Lgth),
    Lgth =:= 7,
    X1 + X2 + X3 =:= 16,
    X2 + X3 + X4 =:= 15,
    X3 + X5 + X6 =:= 19,
    X3 + X4 + X6 + X7 =:= 14.

%    ?- solve_sujiko_2([X1,X2,X3,X4,X5,X6,X7]).
%    X1 = 2,
%    X2 = 8,
%    X3 = 6,
%    X4 = 1,
%    X5 = 9,
%    X6 = 4,
%    X7 = 3 ;
%    false.


% Q4d
solve_sub(X1, X2, X3, X4, Sum) :- X1 + X2 + X3 + X4 =:= Sum.

new_solve_sujiko2([X11,X12,X22,X23,X31,X32,X33]) :-
%check if all arguments are [1,2,3,4,6,8,9]
    gen_list(7, PossibleNumbers, [1,2,3,4,6,8,9]),
    member(X11, PossibleNumbers),
    member(X12, PossibleNumbers),
    member(X22, PossibleNumbers),
    member(X23, PossibleNumbers),
    member(X31, PossibleNumbers),
    member(X32, PossibleNumbers),
    member(X33, PossibleNumbers),
%check if all arguments are distinct
    gen_list(7, DistinctElements, [X11,X12,X22,X23,X31,X32,X33]),
    length(DistinctElements, Lgth),
    Lgth =:= 7,
%check solutions
    solve_sub(X11,X12,5,X22,21),
    solve_sub(X22,7,X12,X23,22),
    solve_sub(5,X22,X31,X32,24),
    solve_sub(X22,X23,X32,X33,14).







