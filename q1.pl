parent(queenmother,elisabeth). parent(elisabeth,charles).
parent(elisabeth,andrew). parent(elisabeth,anne).
parent(elisabeth,edward). parent(diana,william).
parent(diana,harry). parent(sarah,beatrice).
parent(anne,peter). parent(anne,zara).
parent(george,elisabeth). parent(philip,charles).
parent(philip,andrew). parent(philip,edward).
parent(charles,william). parent(charles,harry).
parent(andrew,beatrice). parent(andrew,eugene).
parent(mark,peter). parent(mark,zara).
parent(william,georgejun). parent(kate,georgejun).
parent(william,charlotte). parent(kate,charlotte).
parent(philip,anne). parent(william,louis).
parent(kate,louis).

female(queenmother).
female(elisabeth).
female(diana).
female(anne).
female(beatrice).
female(charlotte).
female(kate).
female(sarah).
female(zara).

male(charles).
male(andrew).
male(edward).
male(william).
male(harry).
male(peter).
male(george).
male(philip).
male(charles).
male(andrew).
male(eugene).
male(mark).
male(georgejun).
male(louis).

% Q1.1a
the_royal_females([]).
the_royal_females([H|T]) :- female(H), the_royal_females(T).

% Q1.1b
the_royal_males([]).
the_royal_males([H|T]) :- male(H), the_royal_males(T).

% Q1.2
the_royal_family([]).
the_royal_family([H|T]) :- (the_royal_males([H]) ; the_royal_females([H])), the_royal_family(T).

% Q1.3
mother(X, Y) :- female(X), parent(X,Y).

% Q1.4
ancestor(X, Y) :- parent(X,Y).
ancestor(X, Y) :- parent(X, Z), ancestor(Z, Y).

% Q1.5
sibling(X, Y) :- parent(Z, X), parent(Z, Y).

% Q1.6
brother(X, Y) :- sibling(X, Y), male(X).

% Q1.7
%?- ancestor(X, louis).
%X = william ;
%X = kate ;
%X = queenmother ;
%X = elisabeth ;
%X = diana ;
%X = george ;
%X = philip ;
%X = charles ;
%false.

% Q1.8
grandmother(X, Y) :- female(X), parent(X, Z), parent(Z, Y).

%?- grandmother(queenmother, Y).
%Y = charles ;
%Y = andrew ;
%Y = anne ;
%Y = edward.

% Q1.9
common_ancestors([], X).
common_ancestors([H|T], X) :- ancestor(H, X), common_ancestors(T, X).

%?- common_ancestors([anne,edward], X).
%false.

%?- common_ancestors([diana, charles],X).
%X = william ;
%X = harry ;
%X = georgejun ;
%X = charlotte ;
%X = louis ;
%false.

% Q1.10
has_brother_who_is_granddad(X) :- brother(X, Y), male(Y), parent(Y, A), parent(A, B).




