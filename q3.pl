% Q3a
euclidsqr([], [], 0).
euclidsqr([A|AL], [B|BL], NewED) :- euclidsqr(AL, BL, ED), NewED is ((A - B) * (A - B) + ED).

% Q3b
euclidsqr_acc([], [], ACC, ACC).
euclidsqr_acc([A|AL], [B|BL], ACC, ED) :- NEW_ACC is ((A - B) * (A - B) + ACC), euclidsqr_acc(AL, BL, NEW_ACC, ED).